import { Discount } from './../models/discount';
import { DiscountService } from './../services/discount.service';
import { DiscountSearchService } from './../services/discount-search.service';
import { BusinessService } from './../services/business-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prizes',
  templateUrl: './prizes.component.html',
  styleUrls: ['./prizes.component.css'],
  providers: [BusinessService, DiscountSearchService] 
})
export class PrizesComponent implements OnInit {

   successfulDiscounts: any = [];

  constructor(private businessService: BusinessService,
    private discountService: DiscountService){}

  ngOnInit()  {
    this.discountService.getDiscounts().subscribe(discounts => {
      //console.log(discounts);
      discounts = discounts.filter(
        discount => discount.likesSpent === discount.likesToAchieve);
        console.log(discounts)
        for (let discount of discounts) {
          var businessDiscount: any;
          this.businessService.getBusiness(discount.businessID).then(
            business => {businessDiscount = [discount, business];
            this.successfulDiscounts.push(businessDiscount);
            console.log(businessDiscount.discount);
            console.log(businessDiscount[0]['prize']);
            
            }
          )
        }
    })
  
  }

}
